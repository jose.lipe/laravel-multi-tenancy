<?php
/**
 * Created by PhpStorm.
 * User: felipe
 * Date: 24/12/17
 * Time: 18:13
 */

return [
  'model' => \App\Account::class,
  'field_name' => 'subdomain',
  'foreign_key' => 'account_id',
  'route_param' => 'account' ,
  'subdomains_except' => [
      'master'
  ]
];