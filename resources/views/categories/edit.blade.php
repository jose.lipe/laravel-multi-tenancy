@extends('layouts.app')

@section('content')
    <div class="container">
        <h3>Nova categoria</h3>

        {!! Form::model($category, ['url' => routeTenant('categories.update',[$category->id]),'class' => 'form', 'method' => 'PUT']) !!}

        @include('categories._form')

        <div class="form-group">
            {!! Form::submit('Alterar categoria',['class' => 'btn btn-primary']) !!}
        </div>

        {!! Form::close() !!}

    </div>
@endsection