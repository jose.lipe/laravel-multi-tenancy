<?php
/**
 * Created by PhpStorm.
 * User: felipe
 * Date: 24/12/17
 * Time: 20:23
 */

namespace App\Routing;

use Illuminate\Routing\Redirector as RedirectorLaravel;

class Redirector extends RedirectorLaravel
{
    public function routeTenant($name, $params = [], $status = 302, $headers = [])
    {
        $tenantManeger = app(\App\Tenant\TenantManager::class);
        $tenantParam = $tenantManeger->routeParam();
        return $this->route($name,$params+[config('tenant.route_param') => $tenantParam],$status,$headers);
    }
}