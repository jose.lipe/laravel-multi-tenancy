<?php
/**
 * Created by PhpStorm.
 * User: felipe
 * Date: 24/12/17
 * Time: 14:51
 */

namespace App\Scopes;


use Illuminate\Database\Eloquent\Model;

trait TenantModels
{
    protected static function boot()
    {
        parent::boot();

        static::addGlobalScope(new TenantScope());

        static::creating(function (Model $model) {
            $tenantManager = app(\App\Tenant\TenantManager::class);
            if ($tenantManager->getTenant()) {
                $accountId = $tenantManager->getTenant()->id;
                $model->account_id = $accountId;
            }
        });
    }
}