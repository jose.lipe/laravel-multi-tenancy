<?php

namespace App\Http\Middleware;

use App\Tenant\TenantManager;
use Closure;

/**
 * Class DefineAuthGuard
 * @package App\Http\Middleware
 */
class DefineAuthGuard
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $tenantManeger = app(TenantManager::class);
        if(!$tenantManeger->getTenant() && !$tenantManeger->isSubdomainExcept()) {
            abort(404);
        }

        if (!$tenantManeger->isSubdomainExcept()) {
            config([
               'auth.defaults.guard' => 'web_tenants',
               'auth.defaults.passwords' => 'user_accounts'
            ]);
        }

        return $next($request);
    }
}
