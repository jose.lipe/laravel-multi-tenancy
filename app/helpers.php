<?php
/**
 * Created by PhpStorm.
 * User: felipe
 * Date: 24/12/17
 * Time: 19:41
 */
if (!function_exists('routeTenant')) {
    function routeTenant($name, $params = [], $absolute = true) {
        $tenantManeger = app(App\Tenant\TenantManager::class);
        $tenantParam = $tenantManeger->routeParam();
        return route($name, $params+[config('tenant.route_param') => $tenantParam], $absolute);
    }
}

if (!function_exists('layoutTenant')) {
    function layoutTenant() {
        $tenantManeger = app(App\Tenant\TenantManager::class);
        $isSubdomainExcept = $tenantManeger->isSubdomainExcept();
        return !$isSubdomainExcept ? 'layouts.app' : 'layouts.admin';
    }
}
